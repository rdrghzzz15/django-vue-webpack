import Vue from 'vue'
import Reg from './Reg'

Vue.config.productionTip = false

new Vue({
    el: '#reg',
    components: { Reg },
    template: '<Reg/>'
})