from django.shortcuts import render, redirect, get_object_or_404
from braces.views import LoginRequiredMixin
from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate, logout, login
from django.views.generic import View
from dashboard.forms import SignUpForm
# Create your views here.


class HomeView(TemplateView):

    template_name = 'dashboard/home.html'

    def get(self, requests, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        return context


class SignUpView(TemplateView):
    template_name = 'dashboard/signup.html'

    def get_context_data(self, **kwargs):
        context = super(SignUpView, self).get_context_data(**kwargs)
        context['form'] = SignUpForm()
        return context

    def post(self, request, *args, **kwargs):
        form = SignUpForm(self.request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=password)
            login(self.request, user)

            return redirect('home')

        return self.render_to_response({'form': form})

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)
