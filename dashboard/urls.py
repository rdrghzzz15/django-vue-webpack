from django.conf.urls import url
from django.contrib.auth import views as auth_views

from dashboard.views import HomeView, SignUpView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^signup/$', SignUpView.as_view(), name='signup'),
    # url(r'^login/$', auth_views.login,
    #     {'template_name': 'dashboard/login.html'}, name='login'),
    # url(r'^logout/$', auth_views.logout, {'next_page': 'login'},
    #     name='logout'),
    # url(r'^createthread/$', CreateThreadView.as_view(), name='createthread')
]
